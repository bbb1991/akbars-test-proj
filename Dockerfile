FROM jetty:latest
ADD target/rest-api-1.0-SNAPSHOT.war /var/lib/jetty/webapps/root.war
EXPOSE 8080
