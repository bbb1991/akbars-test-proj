package me.bbb1991.testproj.restapi.repository;

import me.bbb1991.testproj.restapi.BaseTestConfig;
import me.bbb1991.testproj.restapi.model.Person;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class PersonRepositoryTest extends BaseTestConfig {

    @Autowired
    private PersonRepository personRepository;

    /**
     * Сохранение нового пользователя в БД
     */
    @Test
    public void savePersonTest() {
        Person person = getNewPerson();
        personRepository.save(person);
        Optional<Person> optionalResult = personRepository.findByEmail(person.getEmail());

        assertTrue(optionalResult.isPresent());

        Person result = optionalResult.get();

        assertNotNull(result.getId());
        assertNotNull(result.getCreatedDate());
    }



}