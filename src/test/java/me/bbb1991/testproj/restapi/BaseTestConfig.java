package me.bbb1991.testproj.restapi;

import me.bbb1991.testproj.restapi.config.ApplicationContext;
import me.bbb1991.testproj.restapi.model.Person;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.UUID;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 21 September 2018
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationContext.class)
@ActiveProfiles("test")
@TestPropertySource("classpath:persistence.properties")
public class BaseTestConfig {

    protected Person getNewPerson() {
        String email = UUID.randomUUID().toString();
        return new Person(email + "@example.com", "qweqwe");
    }
}
