package me.bbb1991.testproj.restapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.bbb1991.testproj.restapi.BaseTestConfig;
import me.bbb1991.testproj.restapi.exception.RestExceptionHandler;
import me.bbb1991.testproj.restapi.model.Person;
import me.bbb1991.testproj.restapi.service.PersonServiceImpl;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PersonControllerTest extends BaseTestConfig {

    @Autowired
    private RestExceptionHandler exceptionHandler;

    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private PersonController personController;

    private MockMvc mockMvc;

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(personController).setControllerAdvice(exceptionHandler).build();
        mapper = new ObjectMapper();
        mapper.configure(ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        mapper.enable(ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        Person person = getNewPerson();

        personService.createOrUpdate(person);
    }

    /**
     * Проверка функций регистраций пользователя с валидными данными
     *
     * @throws Exception
     */
    @Test
    public void registerValidPersonTest() throws Exception {

        String json = "{\"email\": \"qweqwe@example.com\", \"password\": \"qweqwe\"}";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)
                .accept(MediaType.APPLICATION_JSON_UTF8);

        this.mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        ;
    }

    /**
     * Проверка функций регистраций пользователя с пустым email
     * todo invalid password
     *
     * @throws Exception
     */
    @Test
    @Ignore
    public void registerPersonWithEmptyEmail() throws Exception {

        String json = "{\"email\": null, \"password\": \"qweqwe\"}";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(json))
                .accept(MediaType.APPLICATION_JSON_UTF8);
        ;

        this.mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
        ;

    }

    /**
     * Проверка функций регистраций пользователя с невалидным email
     *
     * @throws Exception
     */

    @Test
    @Ignore
    public void registerPersonWithIncorrectEmail() throws Exception {
        String json = "{\"email\": \"non-email-string\", \"password\": \"qweqwe\"}";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(json))
                .accept(MediaType.APPLICATION_JSON_UTF8);

        this.mockMvc.perform(builder)
                .andExpect(status().isBadRequest())
        ;

    }

    /**
     * Проверка функций получение данных пользователя по ID
     *
     * @throws Exception
     */
    @Test
    public void getUserByIdTest() throws Exception {

        Person person = getNewPerson();

        personService.createOrUpdate(person);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/person/1")
                .accept(MediaType.APPLICATION_JSON_UTF8);

        this.mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        ;

    }

    /**
     * Проверка функций обнолвения данных пользователя
     *
     * @throws Exception
     */
    @Test
    public void updatePerson() throws Exception {

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/person/1")
                .accept(MediaType.APPLICATION_JSON_UTF8);

        Person originPerson = getPersonFromRequest(builder);
        System.out.println("ORIGIN PERSON: " + originPerson);

        originPerson.setFirstname("John");
        originPerson.setSurname("Smith");

        builder = MockMvcRequestBuilders.post("/person/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(originPerson));

        Person updatedPerson = getPersonFromRequest(builder);
        assertEquals(originPerson.getFirstname(), updatedPerson.getFirstname());
        assertEquals(originPerson.getLastname(), updatedPerson.getLastname());
    }

    private Person getPersonFromRequest(MockHttpServletRequestBuilder builder) throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(builder)
                .andReturn();

        String personJson = mvcResult.getResponse().getContentAsString();

        return mapper.readValue(personJson, Person.class);
    }

    // todo not exist id
    @Test
//    @Ignore
    public void getUserByInvalidIdTest() throws Exception {

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/person/666")
                .accept(MediaType.APPLICATION_JSON_UTF8);

        this.mockMvc.perform(builder)
                .andExpect(status().isBadRequest())
        ;

    }
    // todo already exist email


}