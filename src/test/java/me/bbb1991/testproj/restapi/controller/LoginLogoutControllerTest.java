package me.bbb1991.testproj.restapi.controller;

import me.bbb1991.testproj.restapi.BaseTestConfig;
import me.bbb1991.testproj.restapi.exception.RestExceptionHandler;
import me.bbb1991.testproj.restapi.security.TokenManager;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.util.AssertionErrors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LoginLogoutControllerTest extends BaseTestConfig {

    @Autowired
    private LoginLogoutController loginLogoutController;

    @Autowired
    private RestExceptionHandler exceptionHandler;

    @Autowired
    private TokenManager tokenManager;

    private MockMvc mockMvc;

    private static final String TOKEN_HEADER = "X-Auth-Token";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(loginLogoutController).setControllerAdvice(exceptionHandler).build();
    }

    /**
     * Проверка функционала входа по логину/паролю
     *
     * @throws Exception
     */
    @Test
    public void validLogin() throws Exception {
        String json = "{\"email\": \"admin@example.com\", \"password\": \"123asd$$\"}";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json);

        mockMvc.perform(builder).andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(result -> AssertionErrors.assertTrue("Token should exist", result.getResponse().getHeader(TOKEN_HEADER) != null))
        ;
    }

    /**
     * Проверка входа по невалидным данным
     *
     * @throws Exception
     */
    @Test
    public void invalidLogin() throws Exception {
        String json = "{\"email\": \"hacker@evil.com\", \"password\": \"123456\"}";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json);

        mockMvc.perform(builder).andDo(MockMvcResultHandlers.print())
                .andExpect(status().isUnauthorized())
                .andExpect(result -> AssertionErrors.assertTrue("Token should not exist", result.getResponse().getHeader(TOKEN_HEADER) == null));

    }

    /**
     * Проверка выхода из системы
     *
     * @throws Exception
     */
    @Test
    public void logout() throws Exception {
        String json = "{\"email\": \"admin@example.com\", \"password\": \"123asd$$\"}";

        MockHttpServletRequestBuilder loginBuilder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json);

        MvcResult res = mockMvc.perform(loginBuilder).andReturn();

        String token = res.getResponse().getHeader(TOKEN_HEADER);

        Assert.notNull(tokenManager.getPerson(token), "Token should be valid");

        MockHttpServletRequestBuilder logoutBuilder = MockMvcRequestBuilders.get("/logout")
                .header(TOKEN_HEADER, token);

        mockMvc.perform(logoutBuilder);

        Assert.isNull(tokenManager.getPerson(token), "Token should be invalid");
    }

}