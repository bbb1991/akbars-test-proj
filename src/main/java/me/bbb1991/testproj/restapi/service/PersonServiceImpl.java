package me.bbb1991.testproj.restapi.service;

import me.bbb1991.testproj.restapi.model.Person;
import me.bbb1991.testproj.restapi.repository.PersonRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 September 2018
 */

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @PostConstruct
    public void postConstruct() {
        this.createOrUpdate(new Person("admin@example.com", "123asd$$"));
    }

    /**
     * Поиск пользователя по email
     *
     * @param email пользователя
     * @return {@link Person} если пользователь был найден, иначе бросается исключение {@link IllegalArgumentException}
     */
    @Override
    public Optional<Person> getByEmail(final String email) {
        return personRepository.findByEmail(email);
    }

    @Override
    public void delete(Person person) {
        personRepository.delete(person);
    }

    /**
     * Поиск пользователя по ID.
     *
     * @param id пользователя
     * @return {@link Person}, если был найден пользователь по данной ID
     */
    @Override
    public Optional<Person> getById(Long id) {
        return personRepository.findById(id);
    }

    /**
     * Сохранение нового пользователя или обновление существующего
     *
     * @param person Инстанс {@link Person}, которую требуется сохранить или обновить
     * @return обновленный инстанс {@link Person}. В случае создания нового пользователя присваивается новый ID
     */
    @Override
    public Person createOrUpdate(Person person) {

        LOGGER.info("Creating new person: {}", person);

        if (person.getId() == null) {
            String pass = person.getPassword();
            person.setPassword(BCrypt.hashpw(pass, BCrypt.gensalt()));
        }

        personRepository.save(person);

        LOGGER.info("New user successfully saved! User ID is: {}", person.getId());
        return person;
    }
}
