package me.bbb1991.testproj.restapi.service;

import me.bbb1991.testproj.restapi.model.Person;

import java.util.Optional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 September 2018
 */

public interface PersonService {

    Person createOrUpdate(Person person);

    Optional<Person> getById(Long id);

    Optional<Person> getByEmail(String email);

    void delete(Person person);
}
