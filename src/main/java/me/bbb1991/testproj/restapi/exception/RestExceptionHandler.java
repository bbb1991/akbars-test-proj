package me.bbb1991.testproj.restapi.exception;

import me.bbb1991.testproj.restapi.model.CustomMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import java.util.List;

/**
 * Handler, который отвечает за обработку ошибки системы и формирует минимальный ответ для
 * пользоваетеля, скрывая детали, к примеру стек трейся, доп инфы и др
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 21 September 2018
 */

@ControllerAdvice
public class RestExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<CustomMessage> handle(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus status = HttpStatus.BAD_REQUEST;

        ResponseEntity<CustomMessage> responseEntity;

        if (ex instanceof MethodArgumentNotValidException) {
            responseEntity = handleNotValidException((MethodArgumentNotValidException) ex, headers, status, request);
        } else if (ex instanceof IllegalArgumentException) {
            responseEntity = handleIllegalArgumentException((IllegalArgumentException) ex, headers, status, request);
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            responseEntity = handleExceptionInternal(ex, null, headers, status, request);
        }
        return responseEntity;
    }

    private ResponseEntity<CustomMessage> handleNotValidException(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        BindingResult bindingResult = ex.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(("Following fields are incorrect: "));

        fieldErrors.forEach(e -> stringBuilder.append(e.getField()).append(": ").append(e.getDefaultMessage()));

        return handleExceptionInternal(ex, new CustomMessage(stringBuilder.toString(), status), headers, status, request);
    }

    private ResponseEntity<CustomMessage> handleIllegalArgumentException(IllegalArgumentException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleExceptionInternal(ex, new CustomMessage(ex.getMessage(), status), headers, status, request);
    }

    private ResponseEntity<CustomMessage> handleExceptionInternal(Exception ex, CustomMessage body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
            LOGGER.error("Unexpected error! ", ex);
            body = new CustomMessage(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            LOGGER.warn("Regular exception ", ex);
        }

        return new ResponseEntity<>(body, headers, status);
    }
}
