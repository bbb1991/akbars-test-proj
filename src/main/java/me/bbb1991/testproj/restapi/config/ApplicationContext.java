package me.bbb1991.testproj.restapi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 September 2018
 */

@Configuration
@Import({WebConfig.class, PersistenceContext.class})
@ComponentScan(basePackages = "me.bbb1991.testproj.restapi")
@PropertySource({"classpath:persistence.properties"})
public class ApplicationContext {
}
