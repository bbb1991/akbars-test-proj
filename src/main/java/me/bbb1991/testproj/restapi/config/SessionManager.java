package me.bbb1991.testproj.restapi.config;

import me.bbb1991.testproj.restapi.model.Person;
import me.bbb1991.testproj.restapi.security.TokenManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * HandlerInterceptor, который перед тем, как пускать запросы в контроллеры, проверяет,
 * пршел ли аутентификацию пользователь, валидный токен
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 September 2018
 */
@Component
public class SessionManager implements HandlerInterceptor {

    public static final String HEADER = "X-Auth-Token";

    private static final Logger LOGGER = LoggerFactory.getLogger(SessionManager.class);

    private final TokenManager tokenManager;

    @Autowired
    public SessionManager(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String token = request.getHeader(HEADER);

        LOGGER.info("Token from request is: {}", token);

        Person person = tokenManager.getPerson(token);
        if (person == null) {
            LOGGER.warn("Token is invalid");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }

        LOGGER.info("Token is valid");
        return true;
    }
}