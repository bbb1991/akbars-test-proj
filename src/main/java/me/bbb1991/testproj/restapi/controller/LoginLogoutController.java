package me.bbb1991.testproj.restapi.controller;

import me.bbb1991.testproj.restapi.model.CustomMessage;
import me.bbb1991.testproj.restapi.model.Person;
import me.bbb1991.testproj.restapi.security.TokenManager;
import me.bbb1991.testproj.restapi.service.PersonServiceImpl;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static me.bbb1991.testproj.restapi.config.SessionManager.HEADER;

/**
 * Контроллер для работы с запросами для входа или выхода из системы
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 September 2018
 */

@RestController
public class LoginLogoutController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginLogoutController.class);

    private final PersonServiceImpl personService;

    private final TokenManager tokenManager;

    public LoginLogoutController(PersonServiceImpl personService, TokenManager tokenManager) {
        this.personService = personService;
        this.tokenManager = tokenManager;
    }

    /**
     * Метод для входа пользователя в систему
     *
     * @param person   с заполненными полями {@link Person#email} и {@link Person#password}
     * @param response
     * @return Информационное сообщение, которое указывает, было ли успешный вход или нет,
     * при успешном входе, в ответ {@param response} добавляется header
     * {@link me.bbb1991.testproj.restapi.config.SessionManager#HEADER}
     * с токеном, которую необходимо отправлять со следующими запросами.
     * @see me.bbb1991.testproj.restapi.config.SessionManager#HEADER
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<CustomMessage> login(@RequestBody Person person, HttpServletResponse response) {

        LOGGER.info("Logging in. Person: {}", person);

        Optional<Person> optional = personService.getByEmail(person.getEmail());

        if (optional.isPresent()) {
            if (BCrypt.checkpw(person.getPassword(), optional.get().getPassword())) {
                String token = tokenManager.createNewToken(optional.get()).getToken();
                response.setHeader(HEADER, token);
                return ResponseEntity.ok(new CustomMessage("Success!", HttpStatus.OK));
            }
            LOGGER.warn("User with email {} found but password mismatch!", person.getEmail());
        }

        LOGGER.warn("Invalid credentials! {}:{}", person.getEmail(), person.getPassword());
        return new ResponseEntity<>(new CustomMessage("Invalid credentials!", HttpStatus.UNAUTHORIZED), HttpStatus.UNAUTHORIZED);
    }

    /**
     * Инвалидирует токен из системы
     *
     * @param request
     */
    @RequestMapping("/logout")
    public void logout(HttpServletRequest request) {

        LOGGER.info("Logging out...");

        String token = request.getHeader(HEADER);
        tokenManager.removeToken(token);
    }

}
