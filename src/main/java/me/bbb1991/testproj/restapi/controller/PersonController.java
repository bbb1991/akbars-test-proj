package me.bbb1991.testproj.restapi.controller;

import me.bbb1991.testproj.restapi.model.CustomMessage;
import me.bbb1991.testproj.restapi.model.Person;
import me.bbb1991.testproj.restapi.security.TokenManager;
import me.bbb1991.testproj.restapi.service.PersonServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

import static me.bbb1991.testproj.restapi.config.SessionManager.HEADER;

/**
 * Контроллер для оперирования данными пользователей.
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 September 2018
 */

@RestController
@RequestMapping("/person")
public class PersonController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);

    private final PersonServiceImpl personService;

    private final TokenManager tokenManager;

    @Autowired
    public PersonController(PersonServiceImpl personService, TokenManager tokenManager) {
        this.personService = personService;
        this.tokenManager = tokenManager;
    }

    /**
     * Получение пользователя по ID
     *
     * @param id ID польззователя, по которому необходимо найти пользователя
     * @return {@link Person}, найденный по {@param id}, либо выкидывается ошибка {@link IllegalArgumentException}
     * @throws IllegalArgumentException
     */

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Person getPersonById(@PathVariable final Long id) {

        LOGGER.info("Trying to get person with ID: {}", id);

        Optional<Person> person = personService.getById(id);

        if (!person.isPresent()) {
            throw new IllegalArgumentException("User with ID " + id + " does not exist!");
        }

        LOGGER.info("Got person from DB: {}", person);

        return person.get();
    }

    /**
     * Создание и сохранение нового пользователя, перед сохранением в БД валидируется поля
     * {@link Person#email} и {@link Person#password}.
     * Для поля email:
     * * должен быть уникальным
     * * должен быть не пустым
     * * должен быть email (сверяется по регулярному выражению)
     * <p>
     * Для поля password:
     *
     * @param person новый пользователь, с заполненными полями {@link Person#email} и {@link Person#password}
     * @return инстанс {@link Person}, с присвоенным ID и с заполненным полем {@link Person#createdDate}, либо ошибка
     * {@link IllegalArgumentException} если какие либо проверки не были успешны
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    @ResponseStatus(HttpStatus.CREATED)
    public Person createPerson(@RequestBody @Valid Person person) {
        LOGGER.info("Creating new person: {}", person);
        return personService.createOrUpdate(person);
    }

    /**
     * Обновление информаций о пользователе
     *
     * @param id
     * @param updatedPerson
     * @return
     */
    @RequestMapping(method = {RequestMethod.POST}, value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public Person updatePerson(@PathVariable Long id, @RequestBody Person updatedPerson) {

        LOGGER.info("Updating new User with ID {}", id);
        Optional<Person> person = personService.getById(id);

        if (!person.isPresent()) {
            throw new IllegalArgumentException("User with ID " + id + " does not exist!");
        }
        return personService.createOrUpdate(updatedPerson);
    }

    /**
     * Получение данных о пользователе, который зашел в систему
     *
     * @param request
     * @return
     */
    @RequestMapping("current-user")
    @ResponseStatus(HttpStatus.OK)
    public Person getAuthenticatedPerson(HttpServletRequest request) {

        String token = request.getHeader(HEADER);

        Optional<Person> optional = personService.getByEmail(tokenManager.getPerson(token).getEmail());

        return optional.get();
    }

    /**
     * Удаление пользователя по ID
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public CustomMessage deletePerson(@PathVariable Long id) {

        Optional<Person> optional = personService.getById(id);

        if (!optional.isPresent()) {
            return new CustomMessage("User with ID " + id + " does not exist!", HttpStatus.NOT_FOUND);
        }

        personService.delete(optional.get());

        return new CustomMessage("User with ID " + id + " successfully deleted!", HttpStatus.OK);

    }
}
