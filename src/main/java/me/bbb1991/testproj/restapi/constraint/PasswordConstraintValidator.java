package me.bbb1991.testproj.restapi.constraint;


import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 September 2018
 */

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {
    @Override
    public void initialize(ValidPassword constraintAnnotation) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                new LengthRule(8, 64), // password length: between 8-64
                new CharacterRule(EnglishCharacterData.UpperCase, 1), // at least 1 upper case
                new CharacterRule(EnglishCharacterData.LowerCase, 1), // at least 1 lower case
                new CharacterRule(EnglishCharacterData.Digit, 1), // at least 1 digit
                new CharacterRule(EnglishCharacterData.Special, 1), // at least 1 special symbol
                new WhitespaceRule() // no whitespace
        ));

        RuleResult result = validator.validate(new PasswordData(password));

        if (result.isValid()) {
            return true;
        }

        List<String> messages = validator.getMessages(result);
        String messageTemplate = String.join(",", messages);
        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
        return false;
    }
}