package me.bbb1991.testproj.restapi.security;

import me.bbb1991.testproj.restapi.model.Person;

import java.util.Collection;
import java.util.Map;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 September 2018
 */

public interface TokenManager {

    /**
     * Метод для создания нового токена и привязки к пользователю
     */
    TokenInfo createNewToken(Person person);

    /**
     * Удаление всех токенов пользователя
     */
    void removePerson(Person person);

    /**
     * Удаление выбранного токена
     */
    Person removeToken(String token);

    /**
     * Получение информаций о пользователе, который привязан к текущему пользователю
     */
    Person getPerson(String token);

    /**
     * Получение всех токенов пользователя
     */
    Collection<TokenInfo> getPersonTokens(Person person);

    /**
     * Получение всех текущих активных токенов и пользователей
     */
    Map<String, Person> getValidPersons();
}
