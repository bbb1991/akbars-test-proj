package me.bbb1991.testproj.restapi.security;

import me.bbb1991.testproj.restapi.model.Person;

import java.util.Date;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 September 2018
 */

public final class TokenInfo {

    private final long created = System.currentTimeMillis();
    private final String token;
    private final Person userDetails;
    // TODO expiration etc

    public TokenInfo(String token, Person userDetails) {
        this.token = token;
        this.userDetails = userDetails;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "TokenInfo{" +
                "token='" + token + '\'' +
                ", userDetails" + userDetails +
                ", created=" + new Date(created) +
                '}';
    }
}