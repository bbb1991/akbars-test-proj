package me.bbb1991.testproj.restapi.security;

import me.bbb1991.testproj.restapi.model.Person;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 September 2018
 */

@Component
public class HashMapBasedTokenManager implements TokenManager {

    private final Map<String, Person> validUsers;

    private final Map<Person, TokenInfo> tokens;

    public HashMapBasedTokenManager() {
        validUsers = new HashMap<>();
        tokens = new HashMap<>();
    }

    @Override
    public TokenInfo createNewToken(Person userDetails) {
        String token;
        do {
            token = generateToken();
        } while (validUsers.containsKey(token));

        TokenInfo tokenInfo = new TokenInfo(token, userDetails);
        removePerson(userDetails);
        Person previous = validUsers.put(token, userDetails);
        if (previous != null) {
            return null;
        }
        tokens.put(userDetails, tokenInfo);

        return tokenInfo;
    }

    private String generateToken() {
        byte[] tokenBytes = new byte[32];
        new SecureRandom().nextBytes(tokenBytes);
        return BCrypt.hashpw(new String(tokenBytes), BCrypt.gensalt());
    }

    @Override
    public void removePerson(Person person) {
        TokenInfo token = tokens.remove(person);
        if (token != null) {
            validUsers.remove(token.getToken());
        }
    }

    @Override
    public Person removeToken(String token) {
        Person person = validUsers.remove(token);
        if (person != null) {
            tokens.remove(person);
        }
        return person;
    }

    @Override
    public Person getPerson(String token) {
        return validUsers.get(token);
    }

    @Override
    public Collection<TokenInfo> getPersonTokens(Person userDetails) {
        return Collections.singletonList(tokens.get(userDetails));
    }

    @Override
    public Map<String, Person> getValidPersons() {
        return Collections.unmodifiableMap(validUsers);
    }
}
