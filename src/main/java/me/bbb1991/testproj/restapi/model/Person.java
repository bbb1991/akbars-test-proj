package me.bbb1991.testproj.restapi.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import me.bbb1991.testproj.restapi.constraint.ValidPassword;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.util.Calendar;

/**
 * Модель пользователя. Названа Person, потому что название User конфликтует с существующим
 * в БД системной таблицей user
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 September 2018
 */
@Entity
public class Person {


    // todo avatar?

    private static final int NAME_MAX_SIZE = 60;

    @Id
    @GeneratedValue
    private Long id;

    @Size(max = NAME_MAX_SIZE)
    private String surname;

    @Size(max = NAME_MAX_SIZE)
    private String firstname;

    @Size(max = NAME_MAX_SIZE)
    private String lastname;

    @JsonIgnore
    @PastOrPresent
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createdDate;

    @JsonIgnore
    @PastOrPresent
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar lastModifiedDate;

    @NotEmpty
    @Email
    @Column(unique = true)
    private String email;

    @NotEmpty
    @JsonIgnore
    @ValidPassword
    private String password;

    // hibernate required
    public Person() {
    }

    @JsonCreator
    public Person(@JsonProperty("email") String email, @JsonProperty("password") String password) {
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Calendar getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Calendar createdDate) {
        this.createdDate = createdDate;
    }

    public Calendar getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Calendar lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
