package me.bbb1991.testproj.restapi.model;

import org.springframework.http.HttpStatus;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 21 September 2018
 */

public class CustomMessage {

    private final String message;
    private final int errorCode;
    private final String errorMessage;

    public CustomMessage(String message, HttpStatus status) {
        this.message = message;
        this.errorCode = status.value();
        this.errorMessage = status.getReasonPhrase();
    }

    public String getMessage() {
        return message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
